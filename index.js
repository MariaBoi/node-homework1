const express = require("express");
const app = express();
const port = 8080;

const fs = require("fs");
const morgan = require("morgan");

const urlencodedParser = express.urlencoded({ extended: false });
app.use(express.json());
app.use(morgan("combined"));

app.get("/api/files", (req, res) => {
  let list = [];
  if (fs.existsSync(`${__dirname}/api/files`)) {
    fs.readdirSync(`${__dirname}/api/files/`).forEach((file) => {
      list.push(file);
    });
  } else {
    res.status(400).json({
      message: "Client error",
    });
  }
  res.status(200).json({ message: "Success", files: list });
});

app.get("/api/files/:filename", (req, res) => {
  let filename = req.params.filename;
  fs.stat(`${__dirname}/api/files/${filename}`, (error, stats) => {
    fs.readFile(
      `${__dirname}/api/files/${filename}`,
      "utf8",
      function (err, data) {
        if (err) {
          res.status(400).json({
            message: `No file with ${filename} filename found`,
          });
        } else {
          res.status(200).json({
            message: "Success",
            filename: filename,
            content: data,
            extension: filename.split(".").pop(),
            uploadedDate: stats.ctime,
          });
        }
      }
    );
  });
});

app.post("/api/files", urlencodedParser, (req, res) => {
  if (fs.existsSync(`${__dirname}/api/files/${req.body.filename}`)) {
    res.status(400).json({
      message: "Please specify 'content' parameter",
    });
  } else if (
    (req.body.filename.endsWith(".log") ||
      req.body.filename.endsWith(".txt") ||
      req.body.filename.endsWith(".json") ||
      req.body.filename.endsWith(".yaml") ||
      req.body.filename.endsWith(".xml") ||
      req.body.filename.endsWith(".js")) &&
    req.body.filename &&
    req.body.content
  ) {
    if (!fs.existsSync(`${__dirname}/api/files`)) {
      fs.mkdirSync(`${__dirname}/api`);
      fs.mkdirSync(`${__dirname}/api/files`);
    }
    fs.promises.writeFile(
      `${__dirname}/api/files/${req.body.filename}`,
      req.body.content
    );
    res.status(200).json({
      message: "File created successfully",
    });
  } else {
    res.status(400).json({
      message: "Please specify 'content' parameter",
    });
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
